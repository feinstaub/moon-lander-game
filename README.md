Moon-Lander
===========

The original README can be found here: README.txt

Background
----------
This is a Lunar Lander game, see https://en.wikipedia.org/wiki/Lunar_Lander_(video_game_genre)

Other implementations:

* Using Scratch: https://scratch.mit.edu/projects/72798680/
* Web variant: http://moonlander.seb.ly/
