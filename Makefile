# Makefile for test program for game_libs - lunar lander
CFLAGS+=-Wall $(RPM_OPT_FLAGS) `sdl-config --libs --cflags`
LDFLAGS=`sdl-config --libs`
CC=gcc
CXX=g++

LIBS=SDL_image `sdl-config --libs` -lm $(LDFLAGS)

C_FILES=moon_lander.c game_lib.c DT_drawtext.c
OBJ_FILES=moon_lander.o game_lib.o DT_drawtext.o
OUT_FILE=moon-lander

all: game_lib

game_lib: $(OBJ_FILES)
	$(CXX) $(LDFLAGS) -o $(OUT_FILE) $(OBJ_FILES) -l$(LIBS) -lSDL_mixer -lSDL

moon_lander.o: moon_lander.c
	$(CC) $(CFLAGS) -c -o $@ $^

game_lib.o: game_lib.c
	$(CC) $(CFLAGS) -c -o $@ $^

DT_drawtext.o: DT_drawtext.c
	$(CC) $(CFLAGS) -c -o $@ $^

clean:
	rm -f $(OUT_FILE) $(OBJ_FILES)

install:
	./install.sh


